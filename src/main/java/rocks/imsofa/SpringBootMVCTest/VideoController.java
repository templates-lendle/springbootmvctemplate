package rocks.imsofa.SpringBootMVCTest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class VideoController {
    @GetMapping("/video")
    public String indexAction(Model model, @RequestParam String id){
        model.addAttribute("id", id);
        model.addAttribute("nextURL", "/");
        return "video";
    }
}